<?php
	session_start();//indica que se usara una sesion

	if(isset($_GET['cierra'])){//condicionamos para cerrar sesion si recibimos la variable cierra
		session_destroy();//destruye la sesion actual
		header("Location: index.php");//redirecciona al index
	}
?>
<html>
<head>
	<title>Chat con php</title>
<!-- Incluimos libreria de JQuery -->
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<!-- Incluimos libreria de funciones del chat -->
<script type="text/javascript" src="js/funcionesJS.js"></script>
<!-- Incluimos la hoja de estilos Generales -->
<link href="css/estilosGenerales.css" rel="stylesheet" type="text/css">


</head>
<body>

<?php
//valida si ya existe una sesion de usuario abierta
	if(!isset($_SESSION['id_usuario'])){
		include("html/logueo.html");//incluye la vista para logueo
	}else{
		include('php/vistaChat.php');//incluye la vista del chat
	}	
?>
</body>
</html>